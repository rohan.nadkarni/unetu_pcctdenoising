#Written by: Rohan Nadkarni
#This script shows an example of how to evaluate the UnetU model on a test set wFBP reconstruction (data not included in repository).
#For more details on our test sets, please see Section 2 of our manuscript.


import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import torchvision
import torchvision.transforms.functional as TF
import nibabel as nib
import time

batch_size = 8
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#U-net architecture used for UnetU
class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()

        self.conv1 = nn.Conv2d(4,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv1_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.pool1 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))


        self.conv2 = nn.Conv2d(32,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv2_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.pool2 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))
        
        self.conv3 = nn.Conv2d(64,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm3 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv3_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm3_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.pool3 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))

        self.conv4 = nn.Conv2d(128,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm4 = nn.BatchNorm2d(num_features=256,dtype=torch.double)
        self.conv4_2 = nn.Conv2d(256,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm4_2 = nn.BatchNorm2d(num_features=256,dtype=torch.double)

        self.upsamp1 = nn.Upsample(scale_factor=2)
        self.conv5 = nn.Conv2d(384,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm5 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv5_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm5_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)

        self.upsamp2 = nn.Upsample(scale_factor=2)
        self.conv6 = nn.Conv2d(192,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv6_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)

        self.upsamp3 = nn.Upsample(scale_factor=2)
        self.conv7 = nn.Conv2d(96,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv7_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)

        self.conv_out = nn.Conv2d(32,4,kernel_size=(1,1),stride=(1,1),padding='same',dtype=torch.double)


    def forward(self,x):
        c1 = self.conv1(x)
        n1 = self.norm1(c1)
        r1 = F.leaky_relu(n1,negative_slope=0.2)
        c1_2 = self.conv1_2(r1)
        n1_2 = self.norm1_2(c1_2)
        r1_2 = F.leaky_relu(n1_2,negative_slope=0.2)
        p1 = self.pool1(r1_2)

        c2 = self.conv2(p1)
        n2 = self.norm2(c2)
        r2 = F.leaky_relu(n2,negative_slope=0.2)
        c2_2 = self.conv2_2(r2)
        n2_2 = self.norm2_2(c2_2)
        r2_2 = F.leaky_relu(n2_2,negative_slope=0.2)
        p2 = self.pool2(r2_2)

        c3 = self.conv3(p2)
        n3 = self.norm3(c3)
        r3 = F.leaky_relu(n3,negative_slope=0.2)
        c3_2 = self.conv3_2(r3)
        n3_2 = self.norm3_2(c3_2)
        r3_2 = F.leaky_relu(n3_2,negative_slope=0.2)
        p3 = self.pool3(r3_2)

        c4 = self.conv4(p3)
        n4 = self.norm4(c4)
        r4 = F.leaky_relu(n4,negative_slope=0.2)
        c4_2 = self.conv4_2(r4)
        n4_2 = self.norm4_2(c4_2)
        r4_2 = F.leaky_relu(n4_2,negative_slope=0.2)

        u1 = self.upsamp1(r4_2)
        co1 = torch.cat((r3_2,u1),dim=1)

        c5 = self.conv5(co1)
        n5 = self.norm5(c5)
        r5 = F.leaky_relu(n5,negative_slope=0.2)
        c5_2 = self.conv5_2(r5)
        n5_2 = self.norm5_2(c5_2)
        r5_2 = F.leaky_relu(n5_2,negative_slope=0.2)

        u2 = self.upsamp2(r5_2)

        co2 = torch.cat((r2_2,u2),dim=1)
        c6 = self.conv6(co2)
        n6 = self.norm6(c6)
        r6 = F.leaky_relu(n6,negative_slope=0.2)
        c6_2 = self.conv6_2(r6)
        n6_2 = self.norm6_2(c6_2)
        r6_2 = F.leaky_relu(n6_2,negative_slope=0.2)

        u3 = self.upsamp3(r6_2)

        co3 = torch.cat((r1_2,u3),dim=1)
        c7 = self.conv7(co3)
        n7 = self.norm7(c7)
        r7 = F.leaky_relu(n7,negative_slope=0.2)
        c7_2 = self.conv7_2(r7)
        n7_2 = self.norm7_2(c7_2)
        r7_2 = F.leaky_relu(n7_2,negative_slope=0.2)

        c_out = self.conv_out(r7_2)
        
        t_out = F.tanhshrink(c_out)

        return t_out


#Dataset designed for UnetU DataLoader iterations
class CustomDataset(Dataset):
    def __init__(self,U0_stack,Ug_stack,iter_stack,Uhat_iter_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack,Md_stack,flips):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.iter_stack = iter_stack
        self.Uhat_iter_stack = Uhat_iter_stack        
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack
        self.Md_stack = Md_stack
        self.flips = flips

    #Data augmentation via random horizontal and vertical flips of the axial slices and random multiplication of
    #left and right singular vectors by -1.
    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i



    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:]
        iter_i = self.iter_stack[idx,:,:,:]
        Uhat_iter_i = self.Uhat_iter_stack[idx,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]
        Md_i = self.Md_stack[int(set_i.item()),:,:]
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.U0_stack.size(dim=0)


def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask

def make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size):
    twoT = torch.Tensor([[2]])
    sig = torch.Tensor([[fwhm]])/(twoT*torch.sqrt(twoT*torch.log(twoT)))
    rad = int((kernel_size-1)/2)
    dist = torch.arange(-rad,1+rad,1)
    G = torch.exp(-torch.square(dist)/(twoT*sig**2))
    G = G/torch.sum(G)
    Gz = torch.reshape(G,(1,1,kernel_size,1,1)).double()
    Gy = torch.transpose(Gz,2,3)
    Gx = torch.transpose(Gz,2,4)
    return Gx,Gy,Gz


#Function that converts wFBP reconstruction (X) into a U image, as
#described in Section 2.4 of the paper
def makeNormalizedHPSingularVector(X):
    #Inputs:
        #X: The wFBP reconstruction
    
    #Outputs:
        #U0: The absolute 99.95 percentile normalized, high pass filtered left singular vectors from noise variance weighted SVD that are used as input to UnetU network
        #Minv: Diagonal matrix containing reciprocal of absolute 99.95 percentile values in high-pass filtered left singular vectors on its diagonal
        #Ug: Blurred left singular vectors
        #S: Diagonal matrix output by the noise variance weighted SVD
        #Vh: Right singular vectors output by the noise variance weighted SVD
        #Winv: Diagonal matrix containing reciprocal of noise variance weight values at each energy on its diagonal
    
    nx = X.size(dim=0)
    ny = X.size(dim=1)
    nz = X.size(dim=2)
    ne = X.size(dim=3)

    rmask = create_circular_mask(int(nx),int(ny))
    rmask = torch.Tensor(rmask)
    rmask = torch.reshape(rmask,(1,1,nx,ny))
    rmask = rmask.double()
    
    filt1 = torch.Tensor([ [1], [-1] ]).double()
    filt1 = torch.reshape(filt1,(1,1,2,1))
    filt2 = torch.transpose(filt1,2,3)

    Winv = torch.zeros(4,4)
    Winv = Winv.double()

    #compute noise variance values and populate weighting matrix with reciprocal of these values
    sigma = torch.zeros(ne,1) 
    for e in range(ne):
        X_e = X[:,:,int(nz/2),e]
        X_e = torch.reshape(X_e,(1,1,nx,ny))
        HP = 0.5*F.conv2d(F.conv2d(X_e,filt1,padding='same'),filt2,padding='same')
        HP_roi = HP*rmask
        HP_roi = torch.reshape(HP_roi,(-1,))
        HP_roi = HP_roi[HP_roi.nonzero()]
        sigma[e] = torch.median(torch.abs(HP_roi))/0.6745
        Winv[e,e] = 1/(torch.square(sigma[e])) 

    #compute noise variance weighted SVD
    X = torch.reshape(X,(nx*ny*nz,ne))
    U,S,Vh = torch.linalg.svd((torch.matmul(X,Winv)),full_matrices=False) 
    S = torch.diag(S)
    U = torch.reshape(U,(nx,ny,nz,ne))
    U = torch.permute(U,(3,2,0,1))
    U = torch.unsqueeze(U,0)

    fwhm = 10 
    kernel_size = 37 
    Gx,Gy,Gz = make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size)

    #compute matrix of blurred left singular vectors
    Ug = torch.zeros(U.size()) 
    for ee in range(ne):
        U_e = U[:,ee,:,:,:]
        U_e = torch.unsqueeze(U_e,1)
        Ug[:,ee,:,:,:] = F.conv3d(F.conv3d(F.conv3d(U_e,Gz,padding='same'),Gy,padding='same'),Gx,padding='same')

    #high pass filter left singular vectors
    Uhp = U-Ug 
    Uhp = torch.permute(torch.squeeze(Uhp),(2,3,1,0))

    #Divide high pass filtered left singular vectors by absolute 99.95 percentile values
    Uhp_vects = torch.reshape(Uhp,(nx*ny*nz,ne))
    Minv = torch.diag(torch.reciprocal(torch.Tensor(np.nanpercentile(torch.abs(Uhp_vects),99.95,axis=0)).double()))
    U0_vects = torch.matmul(Uhp_vects,Minv)
    U0 = torch.reshape(U0_vects,(nx,ny,nz,ne))
    return U0, Minv, Ug, S, Vh, Winv

        
#Function that converts U-net predicted U image back into X (reconstruction)
def recoverPCDImagesFromUhat(Uhat2, Ugb2, setsb2, Minvb2, Sb2, Vhb2, Winvb2):
    Uhat2 = torch.permute(Uhat2,(0,2,3,1))
    Ugb2 = torch.permute(Ugb2,(0,2,3,1))

    nz = Uhat2.size(dim=0)
    nx = Uhat2.size(dim=1)
    ny = Uhat2.size(dim=2)
    ne = Uhat2.size(dim=3)

    Uhat_vects = torch.reshape(Uhat2,(nz,nx*ny,ne))
    Ugb_vects = torch.reshape(Ugb2,(nz,nx*ny,ne))

    Uhp_vects_rec = torch.bmm(Uhat_vects,torch.inverse(Minvb2))
    U_vects_rec = Uhp_vects_rec + Ugb_vects
    X_vects_rec = torch.bmm(torch.bmm(U_vects_rec,Sb2),Vhb2)
    X_vects_rec = torch.bmm(X_vects_rec,torch.inverse(Winvb2))
    X_rec = torch.reshape(X_vects_rec,(nz,nx,ny,ne))
    X_rec = torch.permute(X_rec,(0,3,1,2)) #shape needed for loss calculation

    return X_rec


start_time = time.time()

print("Generating test set predictions")
model = UNet()
if torch.cuda.device_count() > 1:
    print("Let's use ",torch.cuda.device_count()," GPUs!")
    model = nn.DataParallel(model)

model = model.to(device)
model.load_state_dict(torch.load('models/UnetUCustomLoss_Model.pth'))

pcd_fdk1 = nib.load('data/TestSetFigure9/wFBP/wFBP_450projection.nii.gz')
pcd_fdk1 = pcd_fdk1.get_fdata()
pcd_fdk1 = pcd_fdk1.astype('float64')
pcd_fdk1 = torch.Tensor(pcd_fdk1).double()
#in this case, the volume is already sufficiently cropped
pcd_fdk1 = pcd_fdk1*1000 #Multiply by constant because attenuation values are on order of 10^(-3) voxels^(-1)
U0, Minv, Ug, S, Vh, Winv = makeNormalizedHPSingularVector(pcd_fdk1)
U0 = torch.permute(U0,(2,3,0,1))
Ug = torch.permute(torch.squeeze(Ug),(1,0,2,3))
settest= torch.zeros(U0.size(dim=0))
#Sensitivity matrix
#You don't actually need sensitivity matrix at test time, you just
#add it to the CustomDataset to avoid errors
Md = 1000*torch.Tensor([[10*0.000158847,10*0.0001527,10*9.66397e-05,10*7.22128e-05],[40*3.22395e-05,40*2.48591e-05,40*1.61799e-05,40*1.25871e-05],\
                     [0.002819057,0.002660879,0.002412376,0.002270096]]).double()

Minv = torch.unsqueeze(Minv,dim=0)
S = torch.unsqueeze(S,dim=0)
Vh = torch.unsqueeze(Vh,dim=0)
Winv = torch.unsqueeze(Winv,dim=0)
Md = torch.unsqueeze(Md,dim=0)

#in this case, I am simply plugging the input into the spot where the label is supposed to go
testDat = CustomDataset(U0,Ug,U0,U0,settest,Minv,S,Vh,Winv,Md,flips=False)
testLoader = DataLoader(testDat,batch_size=batch_size)

pred_iter = torch.zeros(U0.size())
pred_iter = pred_iter.to(device)


idx = 0
with torch.no_grad():
    for batch_idx,(U0b, Ugb, U0b2, U0b3, setsb, Minvb, Sb, Vhb, Winvb, Mdb) in enumerate(testLoader):
        print(idx)
        U0b, Ugb, U0b2, U0b3, setsb, Minvb, Sb, Vhb, Winvb, Mdb = U0b.to(device), Ugb.to(device), U0b2.to(device), U0b3.to(device), setsb.to(device), Minvb.to(device), Sb.to(device), Vhb.to(device), Winvb.to(device), Mdb.to(device)
        Uhatb = model(U0b)
        predictX = recoverPCDImagesFromUhat(Uhatb, Ugb, setsb, Minvb, Sb, Vhb, Winvb)

        if(idx+batch_size >= pred_iter.size(dim=0)):
            pred_iter[idx:,:,:,:] = predictX
        else:
            pred_iter[idx:idx+batch_size,:,:,:] = predictX

        del predictX,U0b,Ugb

        idx = idx+batch_size

pred_iter = pred_iter.detach().to('cpu').numpy()
pred_iter = pred_iter/1000 #Revert back to original scale of attenuation values
pred_iter = np.transpose(pred_iter,(2,3,0,1))
saveimg = nib.Nifti1Image(pred_iter,np.eye(4))
savepath = 'data/TestSetFigure9/UnetUCustomLoss/wFBP_450projection_UnetUCustomLossDenoised.nii'
nib.save(saveimg,savepath)

print("Done. Total time:")
print(time.time() - start_time, " seconds")
