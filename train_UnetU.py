#Written by: Rohan Nadkarni

#This script shows an example of how to train the UnetU model (data not included in repository).
#Use of 2 training sets in code is simply for example.
#Actual UnetU training used 10 pairs of wFBP input / Iterative RSKR label at 4 different PCD threshold settings.
#For more details on our training sets, please see Section 2 of our manuscript.

import numpy as np
import random
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import torchvision
import torchvision.transforms.functional as TF
import nibabel as nib

epochs = 2000
batch_size = 8
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#U-net architecture used for UnetU
class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()

        self.conv1 = nn.Conv2d(4,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv1_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm1_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.pool1 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))


        self.conv2 = nn.Conv2d(32,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv2_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm2_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.pool2 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))
        
        self.conv3 = nn.Conv2d(64,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm3 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv3_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm3_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.pool3 = nn.AvgPool2d(kernel_size=(2,2), stride=(2,2),padding=(0,0))

        self.conv4 = nn.Conv2d(128,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm4 = nn.BatchNorm2d(num_features=256,dtype=torch.double)
        self.conv4_2 = nn.Conv2d(256,256,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm4_2 = nn.BatchNorm2d(num_features=256,dtype=torch.double)

        self.upsamp1 = nn.Upsample(scale_factor=2)
        self.conv5 = nn.Conv2d(384,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm5 = nn.BatchNorm2d(num_features=128,dtype=torch.double)
        self.conv5_2 = nn.Conv2d(128,128,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)        
        self.norm5_2 = nn.BatchNorm2d(num_features=128,dtype=torch.double)

        self.upsamp2 = nn.Upsample(scale_factor=2)
        self.conv6 = nn.Conv2d(192,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6 = nn.BatchNorm2d(num_features=64,dtype=torch.double)
        self.conv6_2 = nn.Conv2d(64,64,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm6_2 = nn.BatchNorm2d(num_features=64,dtype=torch.double)

        self.upsamp3 = nn.Upsample(scale_factor=2)
        self.conv7 = nn.Conv2d(96,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7 = nn.BatchNorm2d(num_features=32,dtype=torch.double)
        self.conv7_2 = nn.Conv2d(32,32,kernel_size=(3,3),stride=(1,1),padding='same',dtype=torch.double)
        self.norm7_2 = nn.BatchNorm2d(num_features=32,dtype=torch.double)

        self.conv_out = nn.Conv2d(32,4,kernel_size=(1,1),stride=(1,1),padding='same',dtype=torch.double)


    def forward(self,x):
        c1 = self.conv1(x)
        n1 = self.norm1(c1)
        r1 = F.leaky_relu(n1,negative_slope=0.2)
        c1_2 = self.conv1_2(r1)
        n1_2 = self.norm1_2(c1_2)
        r1_2 = F.leaky_relu(n1_2,negative_slope=0.2)
        p1 = self.pool1(r1_2)

        c2 = self.conv2(p1)
        n2 = self.norm2(c2)
        r2 = F.leaky_relu(n2,negative_slope=0.2)
        c2_2 = self.conv2_2(r2)
        n2_2 = self.norm2_2(c2_2)
        r2_2 = F.leaky_relu(n2_2,negative_slope=0.2)
        p2 = self.pool2(r2_2)

        c3 = self.conv3(p2)
        n3 = self.norm3(c3)
        r3 = F.leaky_relu(n3,negative_slope=0.2)
        c3_2 = self.conv3_2(r3)
        n3_2 = self.norm3_2(c3_2)
        r3_2 = F.leaky_relu(n3_2,negative_slope=0.2)
        p3 = self.pool3(r3_2)

        c4 = self.conv4(p3)
        n4 = self.norm4(c4)
        r4 = F.leaky_relu(n4,negative_slope=0.2)
        c4_2 = self.conv4_2(r4)
        n4_2 = self.norm4_2(c4_2)
        r4_2 = F.leaky_relu(n4_2,negative_slope=0.2)

        u1 = self.upsamp1(r4_2)
        co1 = torch.cat((r3_2,u1),dim=1)

        c5 = self.conv5(co1)
        n5 = self.norm5(c5)
        r5 = F.leaky_relu(n5,negative_slope=0.2)
        c5_2 = self.conv5_2(r5)
        n5_2 = self.norm5_2(c5_2)
        r5_2 = F.leaky_relu(n5_2,negative_slope=0.2)

        u2 = self.upsamp2(r5_2)

        co2 = torch.cat((r2_2,u2),dim=1)
        c6 = self.conv6(co2)
        n6 = self.norm6(c6)
        r6 = F.leaky_relu(n6,negative_slope=0.2)
        c6_2 = self.conv6_2(r6)
        n6_2 = self.norm6_2(c6_2)
        r6_2 = F.leaky_relu(n6_2,negative_slope=0.2)

        u3 = self.upsamp3(r6_2)

        co3 = torch.cat((r1_2,u3),dim=1)
        c7 = self.conv7(co3)
        n7 = self.norm7(c7)
        r7 = F.leaky_relu(n7,negative_slope=0.2)
        c7_2 = self.conv7_2(r7)
        n7_2 = self.norm7_2(c7_2)
        r7_2 = F.leaky_relu(n7_2,negative_slope=0.2)

        c_out = self.conv_out(r7_2)
        
        t_out = F.tanhshrink(c_out)

        return t_out

#Dataset designed for UnetU DataLoader iterations
class CustomDataset(Dataset):
    def __init__(self,U0_stack,Ug_stack,iter_stack,Uhat_iter_stack,set_stack,Minv_stack,S_stack,Vh_stack,Winv_stack,Md_stack,flips):
        self.U0_stack = U0_stack
        self.Ug_stack = Ug_stack
        self.iter_stack = iter_stack
        self.Uhat_iter_stack = Uhat_iter_stack        
        self.set_stack = set_stack
        self.Minv_stack = Minv_stack
        self.S_stack = S_stack
        self.Vh_stack = Vh_stack
        self.Winv_stack = Winv_stack
        self.Md_stack = Md_stack
        self.flips = flips

    #Data augmentation via random horizontal and vertical flips of the axial slices and random multiplication of
    #left and right singular vectors by -1.
    def transform(self,U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i):
        if random.random() > 0.5:
            U0_i = TF.hflip(U0_i)
            Ug_i = TF.hflip(Ug_i)
            iter_i = TF.hflip(iter_i)
            Uhat_iter_i = TF.hflip(Uhat_iter_i)
            
        if random.random() > 0.5:
            U0_i = TF.vflip(U0_i)
            Ug_i = TF.vflip(Ug_i)
            iter_i = TF.vflip(iter_i)
            Uhat_iter_i = TF.vflip(Uhat_iter_i)

        if random.random() > 0.5:
            U0_i = -1*U0_i
            Ug_i = -1*Ug_i
            Uhat_iter_i = -1*Uhat_iter_i
            Vh_i = -1*Vh_i

        return U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i



    def __getitem__(self,idx):
        U0_i = self.U0_stack[idx,:,:,:]
        Ug_i = self.Ug_stack[idx,:,:,:]
        iter_i = self.iter_stack[idx,:,:,:]
        Uhat_iter_i = self.Uhat_iter_stack[idx,:,:,:]
        set_i = self.set_stack[idx]
        Minv_i = self.Minv_stack[int(set_i.item()),:,:]
        S_i = self.S_stack[int(set_i.item()),:,:]
        Vh_i = self.Vh_stack[int(set_i.item()),:,:]
        Winv_i = self.Winv_stack[int(set_i.item()),:,:]
        Md_i = self.Md_stack[int(set_i.item()),:,:]
        
        if self.flips:
            U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i = self.transform(U0_i,Ug_i,iter_i,Uhat_iter_i,Vh_i)

        return U0_i,Ug_i,iter_i,Uhat_iter_i,set_i,Minv_i,S_i,Vh_i,Winv_i,Md_i

    def __len__(self):
        return self.U0_stack.size(dim=0)


def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = (int(w/2), int(h/2))
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask

def make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size):
    twoT = torch.Tensor([[2]])
    sig = torch.Tensor([[fwhm]])/(twoT*torch.sqrt(twoT*torch.log(twoT)))
    rad = int((kernel_size-1)/2)
    dist = torch.arange(-rad,1+rad,1)
    G = torch.exp(-torch.square(dist)/(twoT*sig**2))
    G = G/torch.sum(G)
    Gz = torch.reshape(G,(1,1,kernel_size,1,1)).double()
    Gy = torch.transpose(Gz,2,3)
    Gx = torch.transpose(Gz,2,4)
    return Gx,Gy,Gz

def make1DSeparableGaussianKernelsFor2DFilter(fwhm,kernel_size):
    twoT = torch.Tensor([[2]])
    sig = torch.Tensor([[fwhm]])/(twoT*torch.sqrt(twoT*torch.log(twoT)))
    rad = int((kernel_size-1)/2)
    dist = torch.arange(-rad,1+rad,1)
    G = torch.exp(-torch.square(dist)/(twoT*sig**2))
    G = G/torch.sum(G)
    Gy = torch.reshape(G,(1,1,kernel_size,1)).double().to(device)
    Gx = torch.transpose(Gy,2,3)
    return Gx,Gy

#Function that converts wFBP reconstruction (X) into a U image, as
#described in Section 2.4 of the paper
def makeNormalizedHPSingularVector(X):
    #Inputs:
        #X: The wFBP reconstruction
    
    #Outputs:
        #U0: The absolute 99.95 percentile normalized, high pass filtered left singular vectors from noise variance weighted SVD of wFBP reconstruction that are used as input to UnetU network
        #Minv: Diagonal matrix containing reciprocal of absolute 99.95 percentile values in high-pass filtered left singular vectors on its diagonal
        #Ug: Blurred left singular vectors
        #S: Diagonal matrix output by the noise variance weighted SVD
        #Vh: Right singular vectors output by the noise variance weighted SVD
        #Winv: Diagonal matrix containing reciprocal of noise variance weight values at each energy on its diagonal
    
    nx = X.size(dim=0)
    ny = X.size(dim=1)
    nz = X.size(dim=2)
    ne = X.size(dim=3)

    rmask = create_circular_mask(int(nx),int(ny))
    rmask = torch.Tensor(rmask)
    rmask = torch.reshape(rmask,(1,1,nx,ny))
    rmask = rmask.double()
    
    filt1 = torch.Tensor([ [1], [-1] ]).double()
    filt1 = torch.reshape(filt1,(1,1,2,1))
    filt2 = torch.transpose(filt1,2,3)

    Winv = torch.zeros(4,4)
    Winv = Winv.double()

    #compute noise variance values and populate weighting matrix with reciprocal of these values
    sigma = torch.zeros(ne,1) 
    for e in range(ne):
        X_e = X[:,:,int(nz/2),e]
        X_e = torch.reshape(X_e,(1,1,nx,ny))
        HP = 0.5*F.conv2d(F.conv2d(X_e,filt1,padding='same'),filt2,padding='same')
        HP_roi = HP*rmask
        HP_roi = torch.reshape(HP_roi,(-1,))
        HP_roi = HP_roi[HP_roi.nonzero()]
        sigma[e] = torch.median(torch.abs(HP_roi))/0.6745
        Winv[e,e] = 1/(torch.square(sigma[e])) 

    #compute noise variance weighted SVD
    X = torch.reshape(X,(nx*ny*nz,ne))
    U,S,Vh = torch.linalg.svd((torch.matmul(X,Winv)),full_matrices=False) 
    S = torch.diag(S)
    U = torch.reshape(U,(nx,ny,nz,ne))
    U = torch.permute(U,(3,2,0,1))
    U = torch.unsqueeze(U,0)

    fwhm = 10 
    kernel_size = 37 
    Gx,Gy,Gz = make1DSeparableGaussianKernelsFor3DFilter(fwhm,kernel_size)

    #compute matrix of blurred left singular vectors
    Ug = torch.zeros(U.size()) 
    for ee in range(ne):
        U_e = U[:,ee,:,:,:]
        U_e = torch.unsqueeze(U_e,1)
        Ug[:,ee,:,:,:] = F.conv3d(F.conv3d(F.conv3d(U_e,Gz,padding='same'),Gy,padding='same'),Gx,padding='same')

    #high pass filter left singular vectors
    Uhp = U-Ug 
    Uhp = torch.permute(torch.squeeze(Uhp),(2,3,1,0))

    #Divide high pass filtered left singular vectors by absolute 99.95 percentile values
    Uhp_vects = torch.reshape(Uhp,(nx*ny*nz,ne))
    Minv = torch.diag(torch.reciprocal(torch.Tensor(np.nanpercentile(torch.abs(Uhp_vects),99.95,axis=0)).double()))
    U0_vects = torch.matmul(Uhp_vects,Minv)
    U0 = torch.reshape(U0_vects,(nx,ny,nz,ne))
    return U0, Minv, Ug, S, Vh, Winv

        
#Function that converts U-net predicted U image back into X (reconstruction)
def recoverPCDImagesFromUhat(Uhat2, Ugb2, setsb2, Minvb2, Sb2, Vhb2, Winvb2):
    Uhat2 = torch.permute(Uhat2,(0,2,3,1))
    Ugb2 = torch.permute(Ugb2,(0,2,3,1))

    nz = Uhat2.size(dim=0)
    nx = Uhat2.size(dim=1)
    ny = Uhat2.size(dim=2)
    ne = Uhat2.size(dim=3)

    Uhat_vects = torch.reshape(Uhat2,(nz,nx*ny,ne))
    Ugb_vects = torch.reshape(Ugb2,(nz,nx*ny,ne))

    Uhp_vects_rec = torch.bmm(Uhat_vects,torch.inverse(Minvb2))
    U_vects_rec = Uhp_vects_rec + Ugb_vects
    X_vects_rec = torch.bmm(torch.bmm(U_vects_rec,Sb2),Vhb2)
    X_vects_rec = torch.bmm(X_vects_rec,torch.inverse(Winvb2))
    X_rec = torch.reshape(X_vects_rec,(nz,nx,ny,ne))
    X_rec = torch.permute(X_rec,(0,3,1,2)) #shape needed for loss calculation

    return X_rec


#Function that converts iterative RSKR label to U domain of wFBP input
def makeUhatLabelFromFDKMatrices(pcd_iter,Minv,Ug,S,Vh,Winv):
    pcd_iter = torch.permute(pcd_iter,(2,3,0,1))
    Ug = torch.permute(Ug,(2,3,0,1))
    nx = pcd_iter.size(dim=0)
    ny = pcd_iter.size(dim=1)
    nz = pcd_iter.size(dim=2)
    ne = pcd_iter.size(dim=3)
    pcd_iter_vects = torch.reshape(pcd_iter,(nx*ny*nz,ne))
    Ug_vects = torch.reshape(Ug,(nx*ny*nz,ne))
    X_w_iter_vects = torch.matmul(pcd_iter_vects,Winv)
    U_iter_vects = torch.matmul(torch.matmul(X_w_iter_vects,torch.inverse(Vh)),torch.inverse(S))
    Uhp_iter_vects = U_iter_vects - Ug_vects
    Uhat_iter_vects = torch.matmul(Uhp_iter_vects,Minv)
    Uhat_iter = torch.reshape(Uhat_iter_vects,(nx,ny,nz,ne))
    Uhat_iter = torch.permute(Uhat_iter,(2,3,0,1))
    return Uhat_iter


#Function called when training UnetU with MSE Loss
def MSEAfterInvertingPreprocess(Uhat1, Ugb1, X_trueb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1):
    X_pred1 = recoverPCDImagesFromUhat(Uhat1, Ugb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1)
    loss = torch.mean(torch.square(X_pred1-X_trueb1))
    return loss


#Function called when training UnetU with Custom Loss
def MSEofXandUandCPlusMSEofXBlurred(Uhat1, Ugb1, X_trueb1, Uhat_iterb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1, Mdb1, lambd1, lambd2, lambd3):
    X_pred1 = recoverPCDImagesFromUhat(Uhat1, Ugb1, setsb1, Minvb1, Sb1, Vhb1, Winvb1)

    C_pred1 = recoverDecompFromX(X_pred1, Mdb1)
    C_trueb1 = recoverDecompFromX(X_trueb1, Mdb1)

    msex_term = torch.mean(torch.square(X_pred1-X_trueb1))

    mseu_term = lambd1*torch.mean(torch.square(Uhat1-Uhat_iterb1))
    
    msec_term = lambd2*torch.mean(torch.square(C_pred1-C_trueb1))

    fwhm = 5
    kernel_size = 19
    Gx,Gy = make1DSeparableGaussianKernelsFor2DFilter(fwhm,kernel_size)
    X_pred1_blur = torch.zeros(X_pred1.size()).double().to(device)
    X_trueb1_blur = torch.zeros(X_trueb1.size()).double().to(device)
    ne = X_pred1.size(dim=1)
    for ee in range(ne):
        X_pred1_e = X_pred1[:,ee,:,:]
        X_pred1_e = X_pred1_e.to(device)
        X_pred1_e = torch.unsqueeze(X_pred1_e,1)
        X_pred1_blur[:,ee:ee+1,:,:] = F.conv2d(F.conv2d(X_pred1_e,Gx,padding='same'),Gy,padding='same')

        X_trueb1_e = X_trueb1[:,ee,:,:]
        X_trueb1_e = X_trueb1_e.to(device)
        X_trueb1_e = torch.unsqueeze(X_trueb1_e,1)
        X_trueb1_blur[:,ee:ee+1,:,:] = F.conv2d(F.conv2d(X_trueb1_e,Gx,padding='same'),Gy,padding='same')
    
    msex_blur_term = lambd3*torch.mean(torch.square(X_pred1_blur-X_trueb1_blur))

    loss = msex_term + mseu_term + msec_term + msex_blur_term
    return loss, msex_term, mseu_term, msec_term, msex_blur_term

#Initialize UnetU model
torch.manual_seed(1)
model = UNet()
if torch.cuda.device_count() > 1:
    print("Let's use ",torch.cuda.device_count()," GPUs!")
    model = nn.DataParallel(model)

model = model.to(device)


#Load and preprocess wFBP for training set #1 (thresholds 25, 34, 50, 60 keV)
pcd_fdk1 = nib.load('TrainSet1_wFBPInput.nii')
pcd_fdk1 = pcd_fdk1.get_fdata()
pcd_fdk1 = pcd_fdk1.astype('float64')
pcd_fdk1 = torch.Tensor(pcd_fdk1).double()
pcd_fdk1 = pcd_fdk1[:,:,40:360,:] #crop out axial slices that have only air or edge-of-sample artifacts
pcd_fdk1 = pcd_fdk1*1000 #Multiply by constant because attenuation values are on order of 10^(-3) voxels^(-1)
U0_1, Minv_1, Ug_1, S_1, Vh_1, Winv_1 = makeNormalizedHPSingularVector(pcd_fdk1)
U0_1 = torch.permute(U0_1,(2,3,0,1))
Ug_1 = torch.permute(torch.squeeze(Ug_1),(1,0,2,3))
set1 = torch.zeros(U0_1.size(dim=0))
#I/Ca/H2O/Gd sensitivity matrix for training set #1
Md_1 = 1000*torch.Tensor([[10*0.000158847,10*0.0001527,10*9.66397e-05,10*7.22128e-05],[40*3.22395e-05,40*2.48591e-05,40*1.61799e-05,40*1.25871e-05],\
                     [0.002819057,0.002660879,0.002412376,0.002270096],[10*0.000142146,10*0.000135775,10*0.000158611,10*0.000125181]]).double()


#Load and preprocess wFBP for training set #2 (thresholds 50, 65, 80, 90 keV)
pcd_fdk2 = nib.load('TrainSet2_wFBPInput.nii')
pcd_fdk2 = pcd_fdk2.get_fdata()
pcd_fdk2 = pcd_fdk2.astype('float64')
pcd_fdk2 = torch.Tensor(pcd_fdk2).double()
pcd_fdk2 = pcd_fdk2[:,:,40:360,:] #crop out axial slices that have only air or edge-of-sample artifacts
pcd_fdk2 = pcd_fdk2*1000 #Multiply by constant because attenuation values are on order of 10^(-3) voxels^(-1)
U0_2, Minv_2, Ug_2, S_2, Vh_2, Winv_2 = makeNormalizedHPSingularVector(pcd_fdk2)
U0_2 = torch.permute(U0_2,(2,3,0,1))
Ug_2 = torch.permute(torch.squeeze(Ug_2),(1,0,2,3))
set2 = torch.ones(U0_2.size(dim=0))
#Gd/Ta/Bi/H2O sensitivity matrix for training set #2
Md_2 = 1000*torch.Tensor([[10*0.0001065200428,10*7.384205237e-05,10*5.307199899e-05,10*4.608221352e-05],\
                          [10*8.770239074e-05,10*9.743375704e-05,10*7.432631683e-05,10*6.10961346e-05],\
                          [10*7.300134748e-05,10*6.57323515e-05,10*7.55682122e-05,10*9.145322256e-05],\
                          [0.002334533026,0.002283294918,0.002287131036,0.002285078866]]).double()


U0_all = torch.cat((U0_1, U0_2), dim=0)
Ug_all = torch.cat((Ug_1, Ug_2), dim=0)
print("Sizes of original and blurred left singular vectors arrays")
print(U0_all.size())
print(Ug_all.size())

Minv_all = torch.cat((torch.unsqueeze(Minv_1,dim=0), torch.unsqueeze(Minv_2,dim=0)), axis = 0)
print("Minv_all size")
print(Minv_all.size())

S_all = torch.cat((torch.unsqueeze(S_1,dim=0), torch.unsqueeze(S_2,dim=0)), axis = 0)
print("S_all size")
print(S_all.size())

Vh_all = torch.cat((torch.unsqueeze(Vh_1,dim=0), torch.unsqueeze(Vh_2,dim=0)), axis = 0)
print("Vh_all size")
print(Vh_all.size())

Winv_all = torch.cat((torch.unsqueeze(Winv_1,dim=0), torch.unsqueeze(Winv_2,dim=0)), axis = 0)
print("Winv_all size")
print(Winv_all.size())

set_all = torch.cat((set1,set2),dim=0)
print("set_all size")
print(set_all.size())

Md_all = torch.cat((torch.unsqueeze(Md_1,dim=0), torch.unsqueeze(Md_2,dim=0)), axis = 0)
print("Md_all size")
print(Md_all.size())    

pcd_iter1 = nib.load('TrainSet1_IterativeRSKRLabel.nii')
pcd_iter1 = pcd_iter1.get_fdata()
pcd_iter1 = pcd_iter1.astype('float64')
pcd_iter1 = pcd_iter1*1000 #Same multiplication factor as wFBP
pcd_iter1 = torch.Tensor(pcd_iter1).double()
pcd_iter1 = pcd_iter1[:,:,40:360,:] #same crop as wFBP
pcd_iter1 = torch.permute(pcd_iter1,(2,3,0,1))
Uhat_iter_1 = makeUhatLabelFromFDKMatrices(pcd_iter1,Minv_1,Ug_1,S_1,Vh_1,Winv_1)

pcd_iter2 = nib.load('TrainSet2_IterativeRSKRLabel.nii')
pcd_iter2 = pcd_iter2.get_fdata()
pcd_iter2 = pcd_iter2.astype('float64')
pcd_iter2 = pcd_iter2*1000 #Same multiplication factor as wFBP
pcd_iter2 = torch.Tensor(pcd_iter2).double()
pcd_iter2 = pcd_iter2[:,:,40:360,:] #same crop as wFBP
pcd_iter2 = torch.permute(pcd_iter2,(2,3,0,1))
Uhat_iter_2 = makeUhatLabelFromFDKMatrices(pcd_iter2,Minv_2,Ug_2,S_2,Vh_2,Winv_2)


pcd_iter_all = torch.cat((pcd_iter1,pcd_iter2),dim=0)
print("pcd_iter_all size")
print(pcd_iter_all.size())

Uhat_iter_all = torch.cat((Uhat_iter_1,Uhat_iter_2),dim=0)
print("Uhat_iter_all size")
print(Uhat_iter_all.size())

train_end = int(0.9*U0_all.size(dim=0))

model_input_train = U0_all[0:train_end,:,:,:]
model_input_val = U0_all[train_end:,:,:,:]

model_blur_train = Ug_all[0:train_end,:,:,:]
model_blur_val = Ug_all[train_end:,:,:,:]

model_Xlabel_train = pcd_iter_all[0:train_end,:,:,:]
model_Xlabel_val = pcd_iter_all[train_end:,:,:,:]

model_label_train = Uhat_iter_all[0:train_end,:,:,:]
model_label_val = Uhat_iter_all[train_end:,:,:,:]

set_train = set_all[0:train_end]
set_val = set_all[train_end:]

trainDat = CustomDataset(model_input_train,model_blur_train,model_Xlabel_train,model_label_train,set_train,Minv_all,S_all,Vh_all,Winv_all,Md_all,flips=True)
trainLoader = DataLoader(trainDat,batch_size=batch_size,shuffle=True)

validDat = CustomDataset(model_input_val,model_blur_val,model_Xlabel_val,model_label_val,set_val,Minv_all,S_all,Vh_all,Winv_all,Md_all,flips=True)
validLoader = DataLoader(validDat,batch_size=batch_size,shuffle=True)

print("prepared data loaders")
optimizer = torch.optim.Adam(model.parameters(),lr=0.0002)
criterion = torch.nn.MSELoss()
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min',factor=0.5,patience=10,verbose=True)
lambd1 = 15
lambd2 = 0.2
lambd3 = 1

for epoch in range(1,epochs+1):
    epochstr = "Epoch " + str(epoch+epoch_offset)
    print(epochstr)
    totloss = 0 #total loss in the epoch
    epochloss = 0 #average loss over the mini-batches in the epoch
    validloss = 0

    for batch_idx,(U0b, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb) in enumerate(trainLoader):
        U0b, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb = U0b.to(device), Ugb.to(device), X_trueb.to(device), Uhat_iterb.to(device), setsb.to(device), Minvb.to(device), Sb.to(device), Vhb.to(device), Winvb.to(device), Mdb.to(device)
        optimizer.zero_grad()
        Uhat = model(U0b)

        #UnetU with Custom Loss
        loss, msex_term, mseu_term, msec_term, msex_blur_term = MSEofXandUandCPlusMSEofXBlurred(Uhat, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb, lambd1, lambd2, lambd3)

        #UnetU with MSE Loss
##        loss = MSEAfterInvertingPreprocess(Uhat, Ugb, X_trueb, setsb, Minvb, Sb, Vhb, Winvb)
        
        loss.backward()
        optimizer.step()
        totloss+=loss.item()

    epochloss = totloss/len(trainLoader)
    print("Average training loss in epoch")
    print(epochloss)

    for U0b, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb in validLoader:
        U0b, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb = U0b.to(device), Ugb.to(device), X_trueb.to(device), Uhat_iterb.to(device), setsb.to(device), Minvb.to(device), Sb.to(device), Vhb.to(device), Winvb.to(device), Mdb.to(device)
        Uhat = model(U0b)

        #UnetU with Custom Loss
        loss, msex_term, mseu_term, msec_term, msex_blur_term = MSEofXandUandCPlusMSEofXBlurred(Uhat, Ugb, X_trueb, Uhat_iterb, setsb, Minvb, Sb, Vhb, Winvb, Mdb, lambd1, lambd2, lambd3)

        #UnetU with MSE Loss
##        loss = MSEAfterInvertingPreprocess(Uhat, Ugb, X_trueb, setsb, Minvb, Sb, Vhb, Winvb)
        
        validloss+=loss.item()

    validloss = validloss/len(validLoader)
    print("Average validation loss in epoch")
    print(validloss)

    scheduler.step(validloss)
    if epoch == 1:
        prevval = validloss
    else:
        if validloss >= prevval:
            val_not_decrease+=1
        else:
            val_not_decrease=0
            prevval = validloss
            #save model every time validation loss decreases
            torch.save(model.state_dict(),'UnetUCustomLoss.pth')
            
    print("number of epochs in a row without validation loss decrease:")
    print(val_not_decrease)
    if(val_not_decrease == 20):
        print("reached patience on validation loss, breaking out of training loop")
        break
